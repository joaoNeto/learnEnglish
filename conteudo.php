<?php 
require_once 'autentificacao.php'; 
$ObjAtividade = new Atividade(null,null,null,null,null,null,null);

if($ObjAtividade->buscarUm($_REQUEST["id"]) == FALSE){
	header("location: error.php?mensagem=ID atividade inexistente");
	exit();
}

?>
<html>
<head>

	<title>Learn English</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">	
	<link rel="stylesheet" type="text/css" href="estiloRaiz.css">	

</head>
<body>	

	<div id="menu-lista-atividades">
		<div id="div-perfil-menu-topo">
			<img src="imagens/foto_perfil/icone-perfil.png" id="icone-perfil">
			<font id="font-nome-perfil" ><?php print $_SESSION["nome"]; ?></font>
		</div>
		<a href="classes/Validar.php?acao=sairConta" id="link"><div id="topico-menu-atividade"><div id="texto-topico-menu-atividade"><center>Sair</center></div></div></a>
		<a href="conteudoFeito.php" id="link"><div id="topico-menu-atividade"><div id="texto-topico-menu-atividade"><center>Lições feitas</center></div></div></a>
		<a href="conteudoCriado.php" id="link"><div id="topico-menu-atividade"><div id="texto-topico-menu-atividade"><center>Lições criadas</center></div></div></a>
		<a href="criarConteudo.php" id="link"><div id="topico-menu-atividade"><div id="texto-topico-menu-atividade"><center>Criar lição</center></div></div></a>
		<a href="login.php" id="link"><div id="topico-menu-atividade"><div id="texto-topico-menu-atividade"><center>Inicio</center></div></div></a>
	</div>

	<center>
		<div id="conteudo-preparatorio-prova">
			
			<div id="div-preparatorio-prova">
				<font id="font-titulo-conteudo-prova">Nome da atividade:</font>
				<font id="font-conteudo-prova"><?php print $ObjAtividade->buscarUm($_REQUEST["id"])->nomeAtividade; ?></font>	
				<font id="font-titulo-conteudo-prova">Assunto abordado:</font>
				<font id="font-conteudo-prova"><?php print $ObjAtividade->buscarUm($_REQUEST["id"])->assuntoAbordado; ?></font>
				<font id="font-titulo-conteudo-prova">Feito por:</font>
				<font id="font-conteudo-prova"><?php print $ObjAtividade->buscarUm($_REQUEST["id"])->feitoPor; ?></font>
				<font id="font-titulo-conteudo-prova">Quantidade de questões:</font>
				<font id="font-conteudo-prova"><?php print $ObjAtividade->buscarUm($_REQUEST["id"])->qtdQuestoes; ?></font>
				<font id="font-titulo-conteudo-prova">Pessoas que passaram:</font>
				<font id="font-conteudo-prova"><?php print $ObjAtividade->buscarUm($_REQUEST["id"])->pessoasPassaram; ?></font>
			</div>
			
			<div id="div-fazer-prova">
				<img	src="imagens/icone-avaliacao.png" id="img-avaliacao">
				<input  type="submit" value="Fazer prova" id="botao-submit-largo" style="margin-left:14%;margin-top:5%;">
			</div>
			
			<div id="div-conteudo-prova">
				<?php print $ObjAtividade->buscarUm($_REQUEST["id"])->conteudoAtividade; ?>
			</div>
			
		</div>
	</center>
	
</body>
</html>
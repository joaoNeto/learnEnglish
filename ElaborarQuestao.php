<?php 
require_once 'autentificacao.php'; 
?>
<html>
<head>

	<title>Learn English</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">	
	<link rel="stylesheet" type="text/css" href="estiloRaiz.css">	

</head>
<body>	

	<div id="topo-cadastro">
		<center>
			<img src="imagens/logotipo.png" id="img-logo">
			<!--print $_SESSION["id"]-->
		</center>	
	</div>
	
	<center>
			<div id="div-elaborar-questao">
				<div id="div-elaborar-questao-conteudo">
					<form method="POST" action="classes/validar.php?acao=cadastrarQuestao">
					<div id="div-questao-pergunta">
						<font id="fonte-texto-titulo-questao">Faça a pergunta</font>
						<input type="text" name="pergunta" placeholder="Ex: whats your name?" id="input-texto">
					</div>

					<div id="div-questao-resposta">
						<font id="fonte-texto-titulo-questao">Qual a resposta</font>
						<input type="text" name="resposta" placeholder="Ex: qual o seu nome?" id="input-texto">
					</div>

					
				</div>
				<div id="div-elaborar-questao-rodape">
					<input type="hidden" 	name="id_atividade" 	value="<?php print $_REQUEST["id"]; ?>">				
					<input type="submit"	id="botao-submit-largo"	value="Proxima questão"		style="float:right;">
					
					</form>
					
					<a href="login.php" id="link"><input type="submit"	id="botao-submit-largo"	value="Finalizar Exame"		style="float:left;"></a>
				</div>
			</div>
		
	</center>
	
</body>
</html>
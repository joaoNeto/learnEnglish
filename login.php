<?php 
require_once 'autentificacao.php'; 
$ObjAtividade = new Atividade(null,null,null,null,null,null,null);
?>
<html>
<head>

	<title>Learn English</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">	
	<link rel="stylesheet" type="text/css" href="estiloRaiz.css">	

</head>
<body>	

<div id="menu-lista-atividades">
	<div id="menu-lista-atividades-logotipo"></div>
	<div id="topico-menu-atividade"><div id="texto-topico-menu-atividade"><center>pessoas passaram</center></div></div>
	<div id="topico-menu-atividade"><div id="texto-topico-menu-atividade"><center>Qtd questoes</center></div></div>
	<div id="topico-menu-atividade"><div id="texto-topico-menu-atividade"><center>Feito por</center></div></div>
	<div id="topico-menu-atividade"><div id="texto-topico-menu-atividade"><center>Assunto abordado</center></div></div>
	<div id="topico-menu-atividade"><div id="texto-topico-menu-atividade"><center>Nome da atividade</center></div></div>
</div>

<div id="linha-horizontal-all"></div>

<div id="menu-logado">
	
	<div id="div-area-menu-perfil">
		<div id="foto-perfil"></div>
		<div id="nome-perfil"><center><?php print $_SESSION["nome"];?></center></div>
		<div id="nome-perfil"><center>Pontuação:<?php print $_SESSION["pontuacao"];?></center></div>
		<center><div id="linha-horizontal"></div></center>
		<a href="criarConteudo.php" id="link"><div id="menu-lateral"><center>Criar lição</center></div></a>
		<center><div id="linha-horizontal"></div></center>
		<a href="conteudoCriado.php" id="link"><div id="menu-lateral"><center>Lições criadas</center></div></a>
		<center><div id="linha-horizontal"></div></center>
		<a href="conteudoFeito.php" id="link"><div id="menu-lateral"><center>Lições feitas</center></div></a>
	</div>
	
	<div id="div-area-icones">
		<a href="classes/Validar.php?acao=sairConta"><img src="imagens/icone-desligar.png" id="img-icone-desligar"></a>
		<img src="imagens/icone-configuracao.png" id="img-icone-configurar">
	</div>

</div>

<div id="conteudo-logado">
	<div id="linha-vertical"></div>

	<?php
	foreach($ObjAtividade->buscarTodos() AS $key=>$atributoAtividade){
	?>
	<a href="conteudo.php?id=<?php print $atributoAtividade->id; ?>">
		<div id="linha-lista-atividade">
			<div id="topico-conteudo-atividade"><div id="texto-topico-conteudo-atividade"><center><?php print $atributoAtividade->pessoasPassaram; ?></center></div></div>
			<div id="topico-conteudo-atividade"><div id="texto-topico-conteudo-atividade"><center><?php print $atributoAtividade->qtdQuestoes; ?></center></div></div>
			<div id="topico-conteudo-atividade"><div id="texto-topico-conteudo-atividade"><center><?php print $atributoAtividade->feitoPor; ?></center></div></div>
			<div id="topico-conteudo-atividade"><div id="texto-topico-conteudo-atividade"><center><?php print $atributoAtividade->assuntoAbordado; ?></center></div></div>
			<div id="topico-conteudo-atividade"><div id="texto-topico-conteudo-atividade"><center><?php print $atributoAtividade->nomeAtividade; ?></center></div></div>
		</div>
	</a>
	<?php
	}
	?>
	
</div>

<div id="div-rodape-login">
	<div id="linha-vertical"></div>
	<font id="font-rodape-login"> < anterior </font>
	<font id="font-rodape-login"> proxima >  </font>
	
	<select id="selectTipo" name="organizar_atividade">
		<option value="Mais recente">			Mais recente			</option>
		<option value="Por atividades">			Por atividades			</option>
		<option value="Por autor">				Por autor				</option>
		<option value="Por assunto">			Por assunto				</option>
		<option value="Quantidade de questoes">	Quantidade de questões	</option>
	</select>
	
	<font id="font-rodape-login" style="float:right;">Organizar atividades por: </font>

</div>


</body>
</html>
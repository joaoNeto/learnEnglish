<meta charset="utf-8">
<?php

function __autoload($class_name){
	require_once $class_name.'.php';
}


if($_REQUEST["acao"] == 'inserirConta'){

	$senha = md5($_POST["senha"]);
	$Conta = new Conta($_POST["nome"],$_POST["email"],$senha,0);
	
	if($Conta->inserir()){
		session_start();
		$_SESSION["autentificacao"] = TRUE;
		$_SESSION["email"]			= $_POST["email"];
		$_SESSION["nome"] 			= $_POST["nome"];
		$_SESSION["senha"] 			= $senha;
		$_SESSION["pontuacao"]		= 0;
		header("location: ../login.php");
	}else{
		header("location: ../error.php?mensagem=falha ao cadastrar a conta");
	}

	exit();	

}if($_REQUEST["acao"] == 'entrarConta'){
	
	$senha = md5($_POST["senha-login"]);
	$email = $_POST["email-login"];

	$ObjConta = new Conta(null,null,null,null);

	if($ObjConta->buscarConta($email,$senha) == FALSE){
		header("location: ../error.php?mensagem=Conta inexistente");
		exit();
	}

	session_start();
	
	$_SESSION["autentificacao"] = TRUE;	
	$_SESSION["nome"] 			= $ObjConta->buscarConta($email,$senha)->nome;
	$_SESSION["email"]			= $ObjConta->buscarConta($email,$senha)->email;
	$_SESSION["id"]				= $ObjConta->buscarConta($email,$senha)->id;
	$_SESSION["pontuacao"]		= $ObjConta->buscarConta($email,$senha)->pontuacao;
	
	header("location: ../login.php");
	
	exit();
	
}if($_REQUEST["acao"] == 'sairConta'){
	
	session_start();
	session_unset();
	session_start();
	$_SESSION["autentificacao"] = FALSE;
	header("location: ../index.php");
	
	exit();
}if($_REQUEST["acao"] == 'cadastrarConteudo'){
		
	session_start();	
	$ObjAtividade = new Atividade($_POST["tituloAtividade"],$_POST["assuntoAbordado"],$_POST["conteudoAtividade"],$_SESSION["nome"],$_SESSION["id"],0,0);
	
	if($ObjAtividade->inserir()){
		header("location: ../ElaborarQuestao.php?id=".$ObjAtividade->buscarIdAtividade()->id);
	}else{
		header("location: ../error.php?mensagem=erro ao cadastrar conteudo");
	}
	
	exit();
	
}if($_REQUEST["acao"] == 'cadastrarQuestao'){
	
	$ObjQuestao = new Questao($_POST["id_atividade"],$_POST["pergunta"],$_POST["resposta"]);
	
	if($ObjQuestao->inserir()){
		header("location: ../ElaborarQuestao.php?id=".$_POST["id_atividade"]);
		exit();
	}else{
		header("location: ../error.php?mensagem=erro ao cadastrar questão");
		exit();
	}
	
}else{
	header("location: ../error.php?mensagem=nenhuma acao valida");
	exit();
}
header("location: ../index.php");


?>
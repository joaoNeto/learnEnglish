<?php

require_once 'Crud.php';

class Conta extends Crud{
	
	private $id;
	private $nome;
	private $email;
	private $senha;
	private $pontuacao;
	protected $table = 'conta';

	
	function __construct($nome,$email,$senha,$pontuacao){
		$this->nome			= $nome;
		$this->email		= $email;
		$this->senha		= $senha;
		$this->pontuacao	= $pontuacao;
	}
	
	

	public function inserir(){
		
		$sql  = "INSERT INTO $this->table (nome,email,senha,pontuacao) VALUES (:nome,:email,:senha,:pontuacao)";
		$stmt = ConnBD::prepare($sql);
		$stmt-> bindParam(':nome', $this->nome);
		$stmt-> bindParam(':email', $this->email);
		$stmt-> bindParam(':senha', $this->senha);
		$stmt-> bindParam(':pontuacao', $this->pontuacao);
		
		return $stmt->execute();
		
	}
	
	public function atualizar($id){
		
		$sql  = "UPDATE $this->table SET nome=:nome,email=:email WHERE id=:id";
		$stmt = ConnBD::prepare($sql);
		$stmt-> bindParam(':nome', $this->nome);
		$stmt-> bindParam(':email', $this->email);
		$stmt-> bindParam(':id', $id);
		
		return $stmt->execute();

	}
	
	public function buscarConta($email,$senha){
		
		$sql  = "SELECT * FROM $this->table WHERE email=:email AND senha=:senha";
		$stmt = ConnBD::prepare($sql);
		$stmt-> bindParam(':email', $email);
		$stmt-> bindParam(':senha', $senha);
		$stmt-> execute();
		
		return $stmt->fetch();
		
	}
	
	
	public function setEmail($email){
		$this->email = $email;
	}
		
	public function getEmail(){
		return $this->email;
	}
	
	public function setNome($nome){
		$this->nome = $nome;
	}		

	public function getNome(){
		return $this->nome;
	}

	public function setPontuacao($pontuacao){
		$this->pontuacao = $pontuacao;
	}		

	public function getPontuacao(){
		return $pontuacao->pontuacao;
	}

	public function setSenha($senha){
		$this->senha = $senha;
	}		

	public function getSenha(){
		return $senha->senha;
	}
	
}

?>
<?php

require_once 'ConnBD.php';

abstract class Crud extends ConnBD{

	protected $table;
	
	abstract public function inserir();
	abstract public function atualizar($id);
	
	public function buscarUm($id){
		
		$sql  = "SELECT * FROM $this->table WHERE id=:id";
		$stmt = ConnBD::prepare($sql);
		$stmt-> bindParam(':id', $id, PDO::PARAM_INT);
		$stmt-> execute();
		
		return $stmt->fetch();
		
	}

	public function buscarTodos(){

		$sql  = "SELECT * FROM $this->table ORDER BY id DESC LIMIT 0,10";
		$stmt = ConnBD::prepare($sql);
		$stmt-> execute();
		
		return $stmt->fetchAll();
	
	}

	public function deletarUm($id){

		$sql  = "DELETE FROM $this->table WHERE id=:id";
		$stmt = ConnBD::prepare($sql);
		$stmt-> bindParam(':id', $id, PDO::PARAM_INT);
		
		return $stmt-> execute();
	
	}

}


?>
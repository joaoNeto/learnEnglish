<?php

require_once 'Crud.php';

class Questao extends Crud{
	
	private $id;
	private $id_atividade;
	private $pergunta;
	private $resposta;
	protected $table = 'repositorioquestoes';

	
	function __construct($id_atividade,$pergunta,$resposta){
		$this->id_atividade	= $id_atividade;
		$this->pergunta		= $pergunta;
		$this->resposta		= $resposta;
	}
	
	public function inserir(){
		
		$sql  = "INSERT INTO $this->table (id_atividade,pergunta,resposta) VALUES (:id_atividade,:pergunta,:resposta)";
		$stmt = ConnBD::prepare($sql);
		$stmt-> bindParam(':id_atividade', $this->id_atividade,PDO::PARAM_INT);
		$stmt-> bindParam(':pergunta', $this->pergunta,PDO::PARAM_STR);
		$stmt-> bindParam(':resposta', $this->resposta,PDO::PARAM_STR);
		return $stmt->execute();
		
	}

	public function atualizar($id){
		
		$sql  = "UPDATE $this->table SET nome=:nome,email=:email WHERE id=:id";
		$stmt = ConnBD::prepare($sql);
		$stmt-> bindParam(':nome', $this->nome);
		$stmt-> bindParam(':email', $this->email);
		$stmt-> bindParam(':id', $id);
		
		return $stmt->execute();

	}
	
	public function setId($id){
		$this->id = $id;
	}
		
	public function getId(){
		return $this->id;
	}

}

?>
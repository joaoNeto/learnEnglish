<?php

require_once 'Crud.php';

class Atividade extends Crud{

	protected $table = 'atividade';	
	private $id;
	private $id_user;
	private $nomeAtividade;
	private $assuntoAbordado;
	private $conteudoAtividade;
	private $feitoPor;
	private $qtdQuestoes;
	private $pessoasPassaram;
	
	function __construct($nomeAtividade,$assuntoAbordado,$conteudoAtividade,$feitoPor,$id_user,$qtdQuestoes,$pessoasPassaram){
		$this->nomeAtividade		= $nomeAtividade;
		$this->assuntoAbordado		= $assuntoAbordado;
		$this->conteudoAtividade	= $conteudoAtividade;
		$this->feitoPor				= $feitoPor;		
		$this->id_user				= $id_user;
		$this->qtdQuestoes			= $qtdQuestoes;
		$this->pessoasPassaram		= $pessoasPassaram;
	}

	public function inserir(){
		
		$sql  = "INSERT INTO $this->table (nomeAtividade,assuntoAbordado,conteudoAtividade,feitoPor,id_user,qtdQuestoes,pessoasPassaram) VALUES (:nomeAtividade,:assuntoAbordado,:conteudoAtividade,:feitoPor,:id_user,:qtdQuestoes,:pessoasPassaram)";
		$stmt = ConnBD::prepare($sql);
		$stmt-> bindParam(':nomeAtividade', 	$this->nomeAtividade);
		$stmt-> bindParam(':assuntoAbordado', 	$this->assuntoAbordado);
		$stmt-> bindParam(':conteudoAtividade', $this->conteudoAtividade);
		$stmt-> bindParam(':feitoPor', 			$this->feitoPor);
		$stmt-> bindParam(':id_user', 			$this->id_user);
		$stmt-> bindParam(':qtdQuestoes', 		$this->qtdQuestoes);
		$stmt-> bindParam(':pessoasPassaram', 	$this->pessoasPassaram);
		
		return $stmt->execute();
		
	}
	
	public function atualizar($id){
		
		$sql  = "UPDATE $this->table SET nome=:nome,email=:email WHERE id=:id";
		$stmt = ConnBD::prepare($sql);
		$stmt-> bindParam(':nome', $this->nome);
		$stmt-> bindParam(':email', $this->email);
		$stmt-> bindParam(':id', $id);
		
		return $stmt->execute();

	}	
	
	public function buscarIdAtividade(){
		
		$sql  = "SELECT id FROM $this->table WHERE nomeAtividade=:nomeAtividade AND assuntoAbordado=:assuntoAbordado AND conteudoAtividade=:conteudoAtividade";
		$stmt = ConnBD::prepare($sql);
		$stmt-> bindParam(':nomeAtividade', $this->nomeAtividade,PDO::PARAM_STR,200);
		$stmt-> bindParam(':assuntoAbordado', $this->assuntoAbordado,PDO::PARAM_STR,200);
		$stmt-> bindParam(':conteudoAtividade', $this->conteudoAtividade,PDO::PARAM_STR,5000);
		$stmt-> execute();
		
		return $stmt->fetch();
		

	}
	
	
	public function setNomeAtividade($nomeAtividade){
		$this->nomeAtividade = $nomeAtividade;
	}
		
	public function getNomeAtividade(){
		return $this->nomeAtividade;
	}
	
}

?>
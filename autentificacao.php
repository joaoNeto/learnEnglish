<?php

function __autoload($class_name){
	require_once 'classes/'.$class_name.'.php';
}

session_start();

if($_SESSION["autentificacao"] == FALSE OR !isset($_SESSION["autentificacao"]) ){
	header("location: error.php?mensagem=Erro na autentificação");
	exit();
}

?>
<link rel="shortcut icon" href="imagens/favicon.ico" type="image/x-icon">

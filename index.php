<?php

session_start();

ini_set('display_errors', 0 );
error_reporting(0);

if($_SESSION["autentificacao"] == TRUE){
	header("location: login.php");
	exit();
}else{
$_SESSION["autentificacao"] = FALSE;
}
?>
<html>
<head>

	<title>Learn English</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">	
	<link rel="stylesheet" type="text/css" href="estiloRaiz.css">	
	<link rel="shortcut icon" href="imagens/favicon.ico" type="image/x-icon">
</head>
<body id="body-index">

	<div id="area-login">
		<div id="cadastro-login"><a href="cadastro.php" id="link">Crie uma conta</a></div>
		<center>
			<div id="login">
				<div id="titulo-login">Acesse sua conta</div>
				<form method="POST" action="classes/Validar.php?acao=entrarConta">
					<input type="email"		id="campo-medio"	placeholder="Digite seu email" required	name="email-login">
					<input type="password"	id="campo-medio"	placeholder="Digite sua senha" required	name="senha-login">
					<input type="submit"	id="botao-submit"	value="Entrar">
				</form>
			</div>		
		</center>
	</div>
	
	<div id="rodape">
		<center>
			<img src="imagens/logotipo.png" id="img-logo">
		</center>
	</div>	

</body>
</html>